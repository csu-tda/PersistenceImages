# cdfs_optcy.py
# MIT license 2018
# Francis Motta

#!python
#cython: boundscheck=False, wraparound=False, nonecheck=False, cdivision=True, profile=True, linetrace=True

from libc.math cimport sqrt, erfc, sin, asin, fabs, M_PI, exp

import numpy as np
cimport numpy as np
cimport cython


cpdef double[:] ncdf(double[:] x, double[:] y, double mu_x=0.0, double mu_y=0.0, double sigma_xx=1.0, double sigma_yy=1.0, double sigma_xy=0.0):
    """
    python wrapper for bivariate normal cumulative distribution function, optimized for both zero or nonzero
    covariance (default kernel for persistence images)
    :param x: (n,)-array of x-coordinates at which to evaluate the cdf (upper limit)
    :param y: (n,)-array of y-coordinates at which to evaluate the cdf (upper limit)
    :param mu_x: x-coordinate of mean of bivariate normal
    :param mu_y: y-coordinate of mean of bivariate normal
    :param sigma_xx: variance in x
    :param sigma_yy: variance in y
    :param sigma_xy: covariance of x and y
    :return: P(X <= x, Y <=y)
    """
    if sigma_xy == 0.0:
        return sbvn_cdf(x, y, mu_x=mu_x, mu_y=mu_y, sigma_x=sigma_xx, sigma_y=sigma_yy)
    else:
        return bvn_cdf(x, y, mu_x=mu_x, mu_y=mu_y, sigma_xx=sigma_xx, sigma_yy=sigma_yy, sigma_xy=sigma_xy)


cdef double[:] norm_cdf(double[:] x):
    """
    univariate normal cumulative distribution function with mean 0.0 and standard deviation 1.0
    :param x: (n,) array of values at which to evaluate the cdf (upper limit)
    :return: P(X <= x), for X ~ N[0,1]
    """

    cdef int i
    cdef int n = x.shape[0]
    cdef double[:] ncdf = np.zeros((n,))
    cdef double s = -1 / sqrt(2.0)
    for i in range(n):
        ncdf[i] = 0.5 * erfc(s * x[i])

    return ncdf


cdef double[:] sbvn_cdf(double[:] x, double[:] y, double mu_x=0.0, double mu_y=0.0, double sigma_x=1.0, double sigma_y=1.0):
    """
    standard bivariate normal cumulative distribution function with specified mean and variances
    :param x: (n,)-array of x-coordinates at which to evaluate the cdf (upper limit)
    :param y: (n,)-array of y-coordinates at which to evaluate the cdf (upper limit)
    :param mu_x: x-coordinate of mean of bivariate normal
    :param mu_y: y-coordinate of mean of bivariate normal
    :param sigma_x: variance in x
    :param sigma_y: variance in y
    :return: P(X <= x, Y <=y)
    """

    cdef int i
    cdef int n = x.shape[0]
    cdef double s_x = sqrt(sigma_x)
    cdef double s_y = sqrt(sigma_y)
    cdef double[:] bvncdf = np.zeros((n,))

    for i in range(n):
        x[i] += -mu_x
        x[i] /= s_x
        y[i] += -mu_y
        y[i] /= s_y

    cdef double[:] bvncdf_x = norm_cdf(x)
    cdef double[:] bvncdf_y = norm_cdf(y)

    for i in range(n):
        bvncdf[i] = bvncdf_x[i] * bvncdf_y[i]

    return bvncdf


cdef double[:] bvn_cdf(double[:] x, double[:] y, double mu_x=0.0, double mu_y=0.0, double sigma_xx=1.0, double sigma_yy=1.0, double sigma_xy=0.0):
    """
    bivariate normal cumulative distribution function with specified mean and covariance matrix based on the Matlab
    implementations by Thomas H. Jørgensen (http://www.tjeconomics.com/code/) and Alan Genz
    (http://www.math.wsu.edu/math/faculty/genz/software/matlab/bvnl.m) using the approach described by Drezner
    and Wesolowsky (https://doi.org/10.1080/00949659008811236)
    NOTE: it is assumed that [sigma_xx, sigma_xy; sigma_xy, sigma_yy] is a positive definite matrix
    :param x: (n,)-array of x-coordinates at which to evaluate the cdf (upper limit)
    :param y: (n,)-array of y-coordinates at which to evaluate the cdf (upper limit)
    :param mu_x: x-coordinate of mean of bivariate normal
    :param mu_y: y-coordinate of mean of bivariate normal
    :param sigma_xx: variance in x
    :param sigma_yy: variance in y
    :param sigma_xy: covariance of x and y
    :return: P(X <= x, Y <=y)
    """

    cdef double twopi = 2.0 * M_PI
    cdef double stwopi = sqrt(twopi)
    cdef double fourpi = 4.0 * M_PI

    cdef int i
    cdef int j
    cdef int pm
    cdef int n = x.shape[0]
    cdef double s_xx = sqrt(sigma_xx)
    cdef double s_yy = sqrt(sigma_yy)
    cdef double s_xy = sqrt(sigma_xy)

    cdef int lg
    cdef double[:] wis
    cdef double[:] xis

    cdef double r = sigma_xy / (s_xx * s_yy)
    cdef double asr = asin(r)
    cdef double opmr = (1.0 - r) * (1.0 + r)
    cdef double sopmr = sqrt(opmr)

    if fabs(r) < 0.3:
        lg = 3
        wis = np.array([0.1713244923791705, 0.3607615730481384, 0.4679139345726904])
        xis = np.array([0.9324695142031522, 0.6612093864662647, 0.2386191860831970])

    elif fabs(r) < 0.75:
        lg = 6
        wis = np.array([0.04717533638651177, 0.1069393259953183, 0.1600783285433464,
                        0.2031674267230659, 0.2334925365383547, 0.2491470458134029])
        xis = np.array([0.9815606342467191, 0.9041172563704750, 0.7699026741943050,
                        0.5873179542866171, 0.3678314989981802, 0.1252334085114692])
    else:
        lg = 10
        wis = np.array([0.01761400713915212, 0.04060142980038694, 0.06267204833410906,
                        0.08327674157670475, 0.1019301198172404, 0.1181945319615184,
                        0.1316886384491766, 0.1420961093183821, 0.1491729864726037,
                        0.1527533871307259])
        xis = np.array([0.9931285991850949, 0.9639719272779138, 0.9122344282513259,
                        0.8391169718222188, 0.7463319064601508, 0.6360536807265150,
                        0.5108670019508271, 0.3737060887154196, 0.2277858511416451,
                        0.07652652113349733])

    cdef double[:] nx = np.zeros((n,))
    cdef double[:] ny = np.zeros((n,))
    cdef double[:] hs = np.zeros((n,))
    cdef double[:] hk = np.zeros((n,))
    cdef double[:] rhk8 = np.zeros((n,))
    cdef double[:] rhk16 = np.zeros((n,))
    cdef double[:] bvncdf = np.zeros((n,))
    cdef double[:] xmy = np.zeros((n,))
    cdef double[:] xmy2 = np.zeros((n,))
    cdef double[:] xmya = np.zeros((n,))
    cdef double[:] xmyt = np.zeros((n,))
    cdef double[:] maxxy = np.zeros((n,))
    cdef double[:] ncdfxmyt = np.zeros((n,))
    cdef double[:] ncdfmaxxy = np.zeros((n,))

    cdef double[:] dim = np.ones((lg,))
    cdef double[:] snm = np.zeros((lg,))
    cdef double[:] snp = np.zeros((lg,))
    cdef double[:] snm2 = np.zeros((lg,))
    cdef double[:] snp2 = np.zeros((lg,))
    cdef double[:] ep1 = np.zeros((lg,))
    cdef double[:] sp1 = np.zeros((lg,))
    cdef double[:] asr1 = np.zeros((lg,))
    cdef double[:] rsp = np.zeros((lg,))
    cdef double[:] xisp = np.zeros((lg,))

    for j in range(n):
        x[j] -= mu_x
        x[j] /= s_xx
        x[j] *= -1.0
        y[j] -= mu_y
        y[j] /= s_yy
        y[j] *= -1.0
        nx[j] = -x[j]
        ny[j] = -y[j]

    cdef double[:] norm_cdf_y
    cdef double[:] nnorm_cdf_y
    cdef double[:] nnorm_cdf_x = norm_cdf(nx)

    for j in range(n):
        hk[j] = x[j] * y[j]
        hs[j] = ((x[j] * x[j]) + (y[j] * y[j])) / 2.0

        rhk8[j] = (4.0 - hk[j]) / 8.0
        rhk16[j] = (12.0 - hk[j]) / 16.0

    if fabs(r) < 0.925:
        nnorm_cdf_y = norm_cdf(ny)

        for i in range(lg):
            snm[i] = sin(asr * (1.0 - xis[i]) / 2.0)
            snp[i] = sin(asr * (1.0 + xis[i]) / 2.0)
            snm2[i] = snm[i] * snm[i]
            snp2[i] = snp[i] * snp[i]

        for j in range(n):
            for i in range(lg):
                bvncdf[j] += wis[i] * (exp(((snm[i] * hk[j] - hs[j]) / (1.0 - snm2[i]))) + exp(((snp[i] * hk[j] - hs[j]) / (1.0 - snp2[i]))))
            bvncdf[j] *= asr / fourpi
            bvncdf[j] += nnorm_cdf_x[j] * nnorm_cdf_y[j]

    else:
        if r < 0:
            for j in range(n):
                y[j] *= -1.0
                hk[j] *= -1.0
                rhk8[j] = (4.0 - hk[j]) / 8.0
                rhk16[j] = (12.0 - hk[j]) / 16.0

        if fabs(r) < 1.0:
            for j in range(n):
                xmy[j] = x[j] - y[j]
                xmy2[j] = xmy[j] * xmy[j]
                xmya[j] = fabs(xmy[j])
                xmyt[j] = -xmya[j] / sopmr
            ncdfxmyt = norm_cdf(xmyt)
            for j in range(n):
                ncdfxmyt[j] *= stwopi

            for j in range(n):
                asr = -(xmy2[j] / opmr + hk[j]) / 2.0
                if asr > -100:
                    bvncdf[j] = sopmr * exp(asr) * (1.0 - (rhk8[j] * (xmy2[j] - opmr) * (1.0 - (rhk16[j] * xmy2[j]) / 5.0) / 3.0) + rhk8[j] * rhk16[j] * opmr * opmr / 5.0)

                if hk[j] > -100:
                    bvncdf[j] -= (exp(-hk[j] / 2.0) * ncdfxmyt[j] * xmya[j] * (1.0 - (rhk8[j] * xmy2[j]) * (1.0 - (rhk16[j] * xmy2[j]) / 5.0) / 3.0))

            sopmr = sopmr / 2.0
            for pm in range(-1,2,2):
                for i in range(lg):
                    xisp[i] = (sopmr + sopmr * pm * xis[i]) * (sopmr + sopmr * pm * xis[i])
                    rsp[i] = sqrt(1.0 - xisp[i])

                for j in range(n):
                    for i in range(lg):
                        sp1[i] = 0.0
                        ep1[i] = 0.0
                        asr1[i] = -(xmy2[j] / xisp[i] + hk[j]) / 2.0
                        if asr1[i] > -100:
                            sp1[i] = 1.0 + rhk8[j] * xisp[i] * (1.0 + rhk16[j] * xisp[i])
                            ep1[i] = exp(-hk[j] * (1.0 - rsp[i]) / (2.0 * (1.0 + rsp[i]))) / rsp[i]
                        bvncdf[j] += sopmr * wis[i] * exp(asr1[i]) * (ep1[i] - sp1[i])

            for j in range(n):
                bvncdf[j] /= -twopi

            if r > 0:
                for j in range(n):
                    maxxy[j] = -max(x[j], y[j])
                ncdfmaxxy = norm_cdf(maxxy)
                for j in range(n):
                    bvncdf[j] += ncdfmaxxy[j]

            elif r < 0:
                norm_cdf_y = norm_cdf(y)
                for j in range(n):
                     bvncdf[j] *= -1.0
                     bvncdf[j] += max(0, nnorm_cdf_x[j] - norm_cdf_y[j])

    for j in range(n):
        bvncdf[j] = max(0.0, min(1.0, bvncdf[j]))

    return bvncdf