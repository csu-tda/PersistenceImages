from distutils.core import setup
from Cython.Build import cythonize
import numpy as np

setup(
    name="cdfs_optcy",
    ext_modules = cythonize("cdfs_optcy.pyx",
                            build_dir="build/cython",
                            annotate=True),
    include_dirs = [np.get_include()]
)